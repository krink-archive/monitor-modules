#!/usr/bin/env python

__version__ = 'meminfo.trigger:0.0.0.3'

import sys
sys.dont_write_bytecode = True

import json
jdata = json.load(sys.stdin)

serverid = str(jdata["serverid"])
server = str(jdata["server"])
swappiness = int(jdata["data"]["swappiness"])
SwapTotal = int(jdata["data"]["SwapTotal"])
SwapFree = int(jdata["data"]["SwapFree"])

try:
    MemAvailable = int(jdata["data"]["MemAvailable"])
except KeyError:
    MemAvailable = -1

swapused = SwapTotal - SwapFree
percent = swapused / float(SwapTotal) * 100


import hashlib
def printAlert(ALERT, SUBJECT):
    ALERT += "\n\n" + ' SwapAvailable:  ' + str(SwapFree) +  ' of ' + str(SwapTotal) + '  ' + str(round(percent)) + '% used'
    ALERT += "\n" + ' MemAvailable:  ' + str(MemAvailable)
    ALERT += "\n" + ' Swapiness:  ' + str(swappiness)
    dupecheck = hashlib.md5(ALERT).hexdigest()
    print json.dumps({'dupecheck': str(dupecheck), 'subject':  str(SUBJECT), 'details': str(ALERT)})


alertList = []
SUBJECT = 'Memory Warning'

if swappiness != 1:
    SUBJECT = 'Memory Notice: Misconfiguration swappiness'
    ALERT  = 'Notice: ' + str(server) + ' swappiness ' + str(swappiness)
    ALERT += '\n https://en.wikipedia.org/wiki/Swappiness '
    ALERT += '\n vm.swappiness=0 swap is disabled  '
    ALERT += '\n vm.swappiness=1 Minimum amount of swapping without disabling it entirely  '
    ALERT += '\n vm.swappiness=30 default '
    ALERT += '\n sysctl -w vm.swappiness=1 '
    alertList.append([ALERT,SUBJECT])

if swapused > 1:
   
    if percent > 2 and percent < 5:
        SUBJECT = 'Memory Notice: Swap Usage'
        ALERT =   'Notice: ' + str(server) + ' swapused ' + str(swapused) + '  ' + str(percent) + ' percent'
        alertList.append([ALERT,SUBJECT])

    if percent > 10 and percent < 49:
        SUBJECT = 'Memory Warning: Swap Usage using 10 percent'
        ALERT =   'Warning: ' + str(server) + ' swapused ' + str(swapused) + '  ' + str(percent) + ' percent'
        alertList.append([ALERT,SUBJECT])

    if percent > 50:
        SUBJECT = 'Memory Critical: Swap Usage CRITICAL swapping greater than 50 percent'
        ALERT =   'Critical: ' + str(server) + ' swapused ' + str(swapused) + '  ' + str(percent) + ' percent'
        alertList.append([ALERT,SUBJECT])

if MemAvailable == -1:
    # MemAvailable not available
    # sysctl -w vm.meminfo_legacy_layout=0
    SUBJECT = 'Memory Notice: vm.meminfo_legacy_layout=1'
    ALERT =   'Notice: ' + str(server) + ' vm.meminfo_legacy_layout=1 '
    ALERT +=  '\n kernels before 3.14 (rh6) do not have MemAvailable '
    ALERT +=  '\n https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=34e431b0ae398fc54ea69ff85ec700722c9da773 '
    ALERT +=  '\n Backport "MemAvailable" field to /proc/meminfo in Red Hat Enterprise Linux 6 '
    ALERT +=  '\n https://access.redhat.com/solutions/776393  '
    ALERT +=  '\n sysctl -w vm.meminfo_legacy_layout=0 '
    alertList.append([ALERT,SUBJECT])

    #Q do we really need to do this next part when MemAvailable -1
    try:
        meminfo_legacy_layout = int(jdata["data"]["vm.meminfo_legacy_layout"])
    except KeyError as e:
        #print e
        meminfo_legacy_layout = 1
    #A yes, to exercise the monitor api framework
        
    
if MemAvailable != -1:

    if MemAvailable < 1048576:

        megabytes = MemAvailable / 1024

        if MemAvailable > 524288 and MemAvailable < 1048576:
            SUBJECT = 'Memory Warning: Memory Available less than 1024 MB'
            ALERT =   'Warning: ' + str(server) + ' MemAvailable ' + str(MemAvailable) + '  ' + str(megabytes) + ' megabytes'
            alertList.append([ALERT,SUBJECT])

        if MemAvailable > 131072 and MemAvailable < 524288:
            SUBJECT = 'Memory Critical: Memory Available CRITICAL less than 512 MB'
            ALERT =   'Critical: ' + str(server) + ' MemAvailable ' + str(MemAvailable) + '  ' + str(megabytes) + ' megabytes'
            alertList.append([ALERT,SUBJECT])

        if MemAvailable < 131072:
            SUBJECT = 'Memory Critical: Memory Available CRITICAL less than 128 MB'
            ALERT =   'Super Critical: ' + str(server) + ' MemAvailable ' + str(MemAvailable) + '  ' + str(megabytes) + ' megabytes'
            alertList.append([ALERT,SUBJECT])


print '{'
print '  "alerts": ['

count = len(alertList)
if count == 0:
    print '{"clear_alerts":"true"}'
else:
    for alert in alertList:
        printAlert(alert[0], alert[1])
        count -= 1
        if count:
            print ','

print '            ],'
print '  "store": {}'
print '}'

sys.exit(0)
