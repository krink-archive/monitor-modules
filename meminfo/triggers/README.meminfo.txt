

cat sample-input.json | ./meminfo

/*

You are going to get something like:

{
  "serverid": 432,
  "server": "sdbc2-ca4-01",
  "data": {"swappiness":"1","MemTotal":"16264952","MemFree":"1093736","MemAvailable":"8318148","SwapTotal":"1048572","SwapFree":"1047704","Shmem":"524512","Buffers":"48","Cached":"7583264"},
  "settings": {
  		"min_free":"1024",
  }
  "store": {}
}

*/


/*

You need to output:

{
	"alerts": [
		{
			"dupecheck": "ee67fa4b99c56cb690bdab4d4e021ee3",
			"subject": "",
			"detail": "db-ca1s-03 has only 36MB of memory free:
                     Critical: less than 128 MB available memory 36832"
		}
	],
	"store": {}
}

*/
