
#----

ba (backup admin)
/usr/libexec/monitor/ba

# administration 
client-create
server-create

json stdin
```
cat client-create.json | ./ba
```

#----

bu (backup)
/.backup/bu

# create new backup
```
/.backup/bu create
/.backup/bu push
```

# restore all files from backup
```
/.backup/bu pull
/.backup/bu restore
```

ba, backup administrator, backups automated
bu, backup

