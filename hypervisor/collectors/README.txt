
./hypervisor >SAMPLE/sample.`hostname`.`date --iso`.json

----

Note: does not require root, as virsh does...

```
[krink@vmsb-ca1s-08 ~]$ virsh list --all
 Id    Name                           State
----------------------------------------------------

[krink@vmsb-ca1s-08 ~]$
```

```
[krink@vmsb-ca1s-08 ~]$ ./hypervisor
{
  "hypervisor": {
      "type":"qemu",
      "cpu": {
          "count": "4",
          "type": "x86_64",
          "kernel": "2245882700000000",
          "idle": "6388490870000000",
          "user": "1550149570000000",
          "iowait": "3531520000000"
      },
      "mem": {
          "total":"131008",
          "used":"82104",
          "free":"48904"
      }
  },
  "domains": {
      "dbsum-ca1s-01": {
          "state":"running",
          "cpu": {
              "count":"24",
              "cpu_time":"108636857262528",
              "system_time":"45570090000000",
              "user_time":"29070210000000"
          },
          "mem": {
               "actual":"8388608",
               "swap_in":"0",
               "rss":"6665476"
          },
          "vda": {
              "device":"/dev/mapper/dbsum-ca1s-01",
              "read_requests_issued":"8685",
              "bytes_read":"261363200",
              "write_requests_issued":"313706",
              "bytes_written":"1760964608",
              "number_of_errors":"-1"
          },
          "vdb": {
              "device":"/dev/mapper/dbsum-ca1s-01-data",
              "read_requests_issued":"36546",
              "bytes_read":"1429365760",
              "write_requests_issued":"11227710",
              "bytes_written":"244754958848",
              "number_of_errors":"-1"
          },
          "br40": {
              "mac":"52:54:00:cc:8c:60",
              "read_bytes":"17352241620",
              "read_packets":"60119566",
              "read_errors":"0",
              "read_drops":"0",
              "write_bytes":"18748633605",
              "write_packets":"58400979",
              "write_errors":"0",
              "write_drops":"0"
          }
      },
      "batch-ca1s-02": {
          "state":"running",
          "cpu": {
              "count":"2",
              "cpu_time":"85690465041805",
              "system_time":"37264390000000",
              "user_time":"28797110000000"
          },
          "mem": {
               "actual":"1048576",
               "swap_in":"0",
               "rss":"989508"
          },
          "vda": {
              "device":"/dev/mapper/batch-ca1s-02",
              "read_requests_issued":"9714",
              "bytes_read":"262194688",
              "write_requests_issued":"611025",
              "bytes_written":"3757514752",
              "number_of_errors":"-1"
          },
          "br40": {
              "mac":"52:54:00:f9:04:a1",
              "read_bytes":"6695526561",
              "read_packets":"8947916",
              "read_errors":"0",
              "read_drops":"0",
              "write_bytes":"10475625930",
              "write_packets":"11732810",
              "write_errors":"0",
              "write_drops":"0"
          },
          "br303": {
              "mac":"52:54:00:12:33:44",
              "read_bytes":"138000196",
              "read_packets":"1931601",
              "read_errors":"0",
              "read_drops":"0",
              "write_bytes":"1236",
              "write_packets":"20",
              "write_errors":"0",
              "write_drops":"0"
          }
      },
      "db-ca1s-102": {
          "state":"running",
          "cpu": {
              "count":"8",
              "cpu_time":"103480072574233",
              "system_time":"43690600000000",
              "user_time":"30282190000000"
          },
          "mem": {
               "actual":"8388608",
               "swap_in":"0",
               "rss":"8501628"
          },
          "vda": {
              "device":"/dev/mapper/db-ca1s-102",
              "read_requests_issued":"11338",
              "bytes_read":"280651264",
              "write_requests_issued":"340432",
              "bytes_written":"2108039168",
              "number_of_errors":"-1"
          },
          "vdb": {
              "device":"/dev/mapper/db-ca1s-102-data",
              "read_requests_issued":"87371",
              "bytes_read":"5202366976",
              "write_requests_issued":"17809310",
              "bytes_written":"449087877632",
              "number_of_errors":"-1"
          },
          "br40": {
              "mac":"52:54:00:cf:3d:b4",
              "read_bytes":"7395232600",
              "read_packets":"19695909",
              "read_errors":"0",
              "read_drops":"0",
              "write_bytes":"11419567577",
              "write_packets":"19812131",
              "write_errors":"0",
              "write_drops":"0"
          }
      },

```


