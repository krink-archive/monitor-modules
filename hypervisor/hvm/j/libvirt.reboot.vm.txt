
[root@vmsb1-ca1s-01 ~]# python
Python 2.7.5 (default, Oct 30 2018, 23:45:53)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-36)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import libvirt
>>> conn = libvirt.open("qemu+tcp:///system")
>>> vm = conn.lookupByName("ntp-ca1s-01")
>>> print vm.state(0)
[1, 2]
>>> vm.reboot(0)
0
>>>

