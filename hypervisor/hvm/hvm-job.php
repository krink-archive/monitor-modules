#!/usr/bin/php
<?php

//pull config file
if(!file_exists('/etc/monitor-hypervisor.conf')) fail('Missing config \'/etc/monitor-hypervisor.conf\'');
include '/etc/monitor-hypervisor.conf';
if(!$config) fail('missing $config');

$input = file_get_contents('php://stdin');

$json = json_decode($input);
if(!$json) fail('Invalid json input');

// validate payload
if(!count($json->cluster)) fail('Payload missing \'cluster\' definition');
if(!in_array($json->uri, array('qemu+tcp://'))) fail('\'uri\' missing/unsupported');

if(isset($json->install)) {
	
	// validate install
	if(!preg_match('/^[a-z][a-z0-9\-\_]+$/', $json->install->hypervisor)) fail('Valid \'install.hypervisor\' is required');
	if(!$json->install->os) fail('\'install.os\' is required');
	if(!array_key_exists($json->install->os, $config->os_install_urls)) fail('\'install.os\' not in available list: ' . implode(',', array_keys($config->os_install_urls)));
	if(!preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $json->install->ip)) fail('Valid \'install.ip\' is required');
	if(!preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $json->install->nm)) fail('Valid \'install.nm\' is required');
	if(!preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/', $json->install->gw)) fail('Valid \'install.gw\' is required');
	if(!preg_match('/^\/dev\/[a-z0-9_\/]+$/', $json->install->disk)) fail('Valid \'install.disk\' is required');
	if(!preg_match('/^br\d{1,5},model=virtio,mac=([a-f0-9]{2}:){5}[a-f0-9]{2}$/', $json->install->network)) fail('Value \'install.network\' is required');
	if($json->install->post_install_template && !array_key_exists($json->install->post_install_template, $config->post_install_templates))
		fail('\'install.post_install_template\' not in available list: ' . implode(',', array_keys($config->post_install_templates)));
	
	
	// build payload for hvm command
	$install = new stdClass();
	$install->cluster = $json->cluster;
	$install->uri = $json->uri;
	$install->install = new stdClass();
	$install->install->kickstart = array(
		"#-----BEGIN LINUX KICKSTART-----#",
		"install",
		"firstboot --disable",
		"url --url=" . $config->os_install_urls[$json->install->os],
		"lang en_US.UTF-8",
		"timezone --utc America/Los_Angeles",
		"keyboard us",
		"network --onboot yes --device eth0 --bootproto static --ip {$json->install->ip} --netmask {$json->install->nm} --gateway {$json->install->gw} --noipv6 --hostname={$json->install->hypervisor}",
		"rootpw  FreshInstall2019",
		"firewall --service=ssh",
		"authconfig --enableshadow --passalgo=sha512 --enablepamaccess --enablemkhomedir",
		"selinux --enforcing",
		"bootloader --location=mbr --driveorder=vda --append='crashkernel=auto console=ttyS0,115200'",
		"cmdline",
		"zerombr",
		"clearpart --all --drives=vda",
		"part / --asprimary --grow --size=1 --fstype=xfs",
		"%packages --nobase --ignoremissing",
		"@core",
		"%end",
		"%post --log=/root/install-post-log");
	$replacements = array(
		'[hostname]' => $json->install->hypervisor,
		'[pin]' => $json->install->pin,
	);
	if($json->install->post_install_template) $config->post_install = array_merge($config->post_install, $config->post_install_templates[$json->install->post_install_template]);
	foreach($config->post_install as $line) {
		$install->install->kickstart[] = str_replace(array_keys($replacements), $replacements, $line);
	}
	$install->install->kickstart[] = "%end";
	$install->install->kickstart[] = "reboot";
	$install->install->kickstart[] = "#-----END LINUX KICKSTART-----#";
	
	$install->install->tmpfile = "/tmp/ks.cfg";
	$install->install->cmdline = "virt-install --quiet --noautoconsole --wait=-1 --name={$json->install->hypervisor} " .
		"--vcpus=2 --ram=2048 --disk path={$json->install->disk} " .
		"--network {$json->install->network} -l {$config->os_install_urls[$json->install->os]} " .
		"--initrd-inject {$install->install->tmpfile} --extra-args='inst.ks=file:/ks.cfg " .
		"ip={$json->install->ip} netmask={$json->install->nm} gateway={$json->install->gw} noverifyssl console=tty0 " .
		"console=ttyS0,115200 ipv6.disable=1 net.ifnames=0 biosdevname=0' " .
		($json->install->network2 ? "--network {$json->install->network2}" : '') .
		"--accelerate --nographics &";
	$install->hyperisor = $json->install->hypervisor;
	
	runProc($install);
	exit(0);
}

if(isset($json->migrate)) {
	
	if(!preg_match('/^[a-z][a-z0-9\-\_]+$/', $json->migrate->vm)) fail('Valid \'migrate.vm\' is required');
	if(!preg_match('/^[a-z][a-z0-9\-\_]+$/', $json->migrate->dst)) fail('Valid \'migrate.dst\' is required');
	if(!in_array($json->migrate->dst, $json->cluster)) fail('Destination hypervisor not a member of fault domain');
	
	// build payload for hvm command
	$hvm = new stdClass();
	$hvm->cluster = $json->cluster;
	$hvm->uri = $json->uri;
	$hvm->migrate = new stdClass();
	$hvm->migrate->vm = $json->migrate->vm;
	$hvm->migrate->dst = $json->migrate->dst;
	
	runProc($hvm);
	exit(0);
	
}

if(isset($json->shutdown)) {
	
	if(!preg_match('/^[a-z][a-z0-9\-\_]+$/', $json->shutdown->vm)) fail('Valid \'shutdown.vm\' is required');
	
	// build payload for hvm command
	$hvm = new stdClass();
	$hvm->cluster = $json->cluster;
	$hvm->uri = $json->uri;
	$hvm->shutdown = new stdClass();
	$hvm->shutdown->vm = $json->shutdown->vm;
	
	runProc($hvm);
	exit(0);
	
}
if(isset($json->startup)) {
	
	if(!preg_match('/^[a-z][a-z0-9\-\_]+$/', $json->startup->vm)) fail('Valid \'startup.vm\' is required');
	if(!$json->startup->xml) fail('Valid \'startup.xml\' is required');
	
	// build payload for hvm command
	$hvm = new stdClass();
	$hvm->cluster = $json->cluster;
	$hvm->uri = $json->uri;
	$hvm->startup = new stdClass();
	$hvm->startup->vm = $json->startup->vm;
	$hvm->startup->xml = $json->startup->xml;
	
	runProc($hvm);
	exit(0);
	
}

fail("No valid command found in payload");


function runProc($payload, $args = false)
{
	$descriptorspec = array(
		0 => array('pipe', 'r'),  // stdin is a pipe that the child will read from
		1 => array('pipe', 'w'),  // stdout is a pipe that the child will write to
		2 => array('pipe', 'w'), // stderr is a pipe that the child will write to
	);
	
	$process = proc_open("/usr/libexec/monitor/hvm" . ($args ? ' ' . $args : ''), $descriptorspec, $pipes);
	if(!is_resource($process)) {
		return array('error' => 'Failed to exec module', 'rawoutput' => '');
	}
	
	// write data
	$payload = json_encode($payload, JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);
	//echo "Writing\n------\n$payload\n";
	fwrite($pipes[0], $payload);
	fclose($pipes[0]);
	
	// read response
	$response = stream_get_contents($pipes[1]);
	echo $response;
	//echo "Reading\n------\n$response\n";
	
	fclose($pipes[1]);
	
	// read errors
	$err_response = stream_get_contents($pipes[2]);
	fwrite(STDERR, $err_response);
	//echo "Reading STDERR\n------\n$err_response\n";
	fclose($pipes[2]);
	
	// close process
	$return_value = proc_close($process);
	//echo "command returned $return_value\n";
	
	exit($return_value);
	
}

function fail($mesg)
{
	fwrite(STDERR, $mesg . "\n");
	exit(1);
}


class CommandLineArgs
{
	
	protected $_args;
	protected $_filelist;
	protected $_command;
	
	function __construct($defaults = array())
	{
		global $argv;
		
		$this->_args = array();
		$this->_filelist = array();
		
		foreach($defaults as $k => $v) {
			$this->_args[$k] = $v;
		}
		
		foreach($argv as $arg) {
			if(!$this->_command) {
				$this->_command = $arg;
				continue;
			}
			if(substr($arg, 0, 2) != '--') {
				$this->_filelist[] = $arg;
				continue;
			}
			$arg = substr($arg, 2);
			
			$tmp = preg_split('/ ?= ?/', $arg);
			
			if(!@$tmp[1]) @$tmp[1] = true;
			
			$this->_args[$tmp[0]] = $tmp[1];
			
		}
		
		
	}
	
	function __get($key)
	{
		return $this->_args[$key];
	}
	
	function getFiles()
	{
		return $this->_filelist;
	}
	
	function getCommand()
	{
		return $this->_command;
	}
	
}