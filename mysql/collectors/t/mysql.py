#!/usr/bin/python

__version__ = '0.0.0.0'

dbconf = '/etc/db.conf'
mysql_socket = '/var/lib/mysql/mysql.sock'

import os
import json
import sys
sys.dont_write_bytecode = True

# 1. get db credentials... //this is likey change in the future,
db_user = db_pass = None
if os.path.isfile(dbconf):
    with open(dbconf) as conf:
        for line in conf: #test- try/catch here, or else clause, we've said None for now...
            if line.startswith("define('dbaUser'"): #define('dbaUser','XXXX');
                db_user = line.split(',')[1].strip('\'').split('\'')[0]
            if line.startswith("define('dbaPass'"): #define('dbaPass','XXXX');
                db_pass = line.split(',')[1].strip('\'').split('\'')[0]
else:
    #ls: cannot access /etc/db.conf: No such file or directory
    #python IOError: [Errno 2] No such file or directory: '/etc/db.conf'
    output = {'cannot_access':'No such file or directory: ' + str(dbconf)}
    print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
    sys.exit(1)

# 2. make sure we have the mysql modules...
try: #most systems have this module installed already, but we'll auto install if needed...
    import mysql.connector
except ImportError as e:
    output = {'ImportError':str(e),
              'AutoFix':'yum install mysql-connector-python'} #test- output here
    print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
    try:
        os.system('yum install -y mysql-connector-python') #test- change to subprocess w/ try/catch
        output = {'AutoRun':'yum install -y mysql-connector-python'} #test- output here
        print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
    except Exception as e:
        output = {'Exception':str(e)}
        print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
        sys.exit(1) #test- output here

# 3. our mysql connector info...
try:
    config = {
        'user': db_user,
        'password': db_pass,
        'unix_socket': mysql_socket,
        'database': 'mysql',
        'raise_on_warnings': True,
    }
except UnboundLocalError as e:
    output = {'UnboundLocalError':str(e)} #print 'Error UnboundLocalError ' + str(e)
    if "local variable 'db_user' referenced before assignment" in e:
        #print 'No db_user var ' + str(e)
        output["local_variable"] = "local variable 'db_user' referenced before assignment"
    print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
    sys.exit(1) #test- output here

# 4. try and connect, via unix socket...
if not os.path.exists(mysql_socket):
    output = {'cannot_access':'No such file or directory: ' + str(mysql_socket)}
    print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
    sys.exit(1) # for now just exit(1)...
                #test- verify if mysqld process is up and running without socket file

try:
    cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
    output = {'mysql.connector.Error':str(e)}
    print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
    sys.exit(1) #test- output here

# 5. we've got a cursor, run some sql...
cursor = cnx.cursor(buffered=True)
try:
    #lets start with mysql version.  older 5.X systems syntax differ
    sql = "select version();"
    cursor.execute(sql)
    select_version = cursor.fetchone()

    if str(select_version[0]).startswith("10"):
        sql = "show all slaves status;"
    else:
        sql = "show slave status;"

    #get slave status...  #test- perhaps slave status done last
    cursor.execute(sql)
    if cursor.rowcount > 0:
        show_slave_status = dict(zip(cursor.column_names, cursor.fetchone()))
    else:
        show_slave_status = ''

    #get show status
    sql = "show status;"
    cursor.execute(sql)
    if cursor.rowcount > 0:
        show_status = cursor.fetchall()
    else:
        show_status = ''

    #get show variables
    sql = "show variables;"
    cursor.execute(sql)
    if cursor.rowcount > 0:
        show_variables = cursor.fetchall()
    else:
        show_variables = ''

except mysql.connector.Error as e:
    output = {'mysql.connector.Error':str(e)}
    print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
    sys.exit(1) #test- output here

# 6. close/clean up connections...
cursor.close()
cnx.close()

# 7. work with the data...
# we have select_version fetchone()/tuple
# we have show_slave_status fetchone()/dict
# we have show_status fetchall()/list
# we have show_variables fetchall()/list

output = {}
#select_version is type tuple, [0] is just a string
output['version'] = str(select_version[0])

#show_slave_status is type dict, just add/merge to current output dict...
output.update(show_slave_status)

#show_status is type list
for row in show_status:
    output[row[0]] = str(row[1])

#show_variables is type list
for row in show_variables:
    output[row[0]] = str(row[1])

print json.dumps(output, sort_keys=True, indent=4, separators=(',',': '))
sys.exit(0)
