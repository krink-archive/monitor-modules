
Overview: collect local, send collection to remote.
collect local data and send via https post, where triggers perform tasks

collector scripts output json
collector json is consumed by the monitor process

this.collector
this is the mysql collector...
the mysqld process is running local, /var/lib/mysql/mysql.sock

1. dba credentials.  for now read /etc/db.conf.  this will likely
       change in the future, but use for now... 
           define('dbaUser','XXXX');
           define('dbaPass','XXXX');
  if no db.conf, json out so

2. verify local unix socket /var/lib/mysql/mysql.sock
     if not, is pid running?

3. once unix socket, connect and collect.  send to stdout json
   select version(); show slave status; show status; show variables;

Notes:
Collector scripts should be simple and to the point.  Try providing the best/most fail safe exits and catches
it should be made the attempt for universal usage where possible.  ie, credentials and socket location made
adjustable settings.
 
